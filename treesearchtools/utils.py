__author__ = 's1671845'
import numpy as np


def choice(values, rng, p=None):
    """
    I got tired of writing this over and over again. choice has issues with nested lists/tuples so this is a safe version.
    :param values:
    :param rng:
    :return:
    """

    return values[rng.choice(len(values), p=p)]

def softmax(values, temp=1.0):
    expValues = np.exp(np.divide(values,temp))
    return expValues / np.sum(expValues)