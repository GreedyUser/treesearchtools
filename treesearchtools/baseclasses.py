from collections import Counter

import copy
import numpy as np
from treesearchtools.utils import choice


class StateId:
    def __init__(self, key, polarity=1.0):
        self.__key = key
        self.__polarity = polarity

    @property
    def polarity(self):
        return self.__polarity

    def __eq__(self, other):
        return self.__key == other.__key and self.__polarity == other.__polarity

    def __ne__(self, other):
        return not self.__eq__(other)

    def __hash__(self):
        return hash((self.__key, self.__polarity))

    def __str__(self):
        return "{}:{}".format(self.__polarity, self.__key)

class StateStatistics:
    def __init__(self, stateId: StateId = None, defaultValue=0.0):
        self.stateId = stateId
        self.episodeCount = 0
        self.totalObservedReward = 0.0
        self.actionCounts = Counter()
        self.actionValues = {}
        self.defaultValue = defaultValue
        self.totalSubActionsTaken = 0
        self.heuristicValues = {}
        self.expanded = False
        self._mostTakenActions = None
        self._bestActions = None

    def dirty(self):
        self._mostTakenActions = None
        self._bestActions = None

    def update(self, observedReward, subActionsTaken, actionTaken=None):
        self.dirty()
        self.episodeCount += 1
        if actionTaken != None:
            self.actionCounts[actionTaken] += 1
            try:
                self.actionValues[actionTaken] += observedReward
            except:
                self.actionValues[actionTaken] = observedReward

        self.totalSubActionsTaken += subActionsTaken
        # observedReward = self.stateId.polarity * observedReward
        self.totalObservedReward += observedReward

    def unvisitedActions(self, actions):
        unvisitedActions = []
        for action in actions:
            if action not in self.actionCounts:
                unvisitedActions.append(action)
        return unvisitedActions

    def estimatedQ(self, action, **kwargs):
        """

        :param action:
        :param kwargs: if 'qDefault' is defined it will be returned if the action hasn't been observed yet otherwise self.estimatedValue is used.
        :return:
        """
        try:
            if action not in self.actionCounts:
                return kwargs["qDefault"]

            return self.actionValues[action] / self.actionCounts[action]
        except:
            return self.estimatedValue

    @property
    def estimatedValue(self):
        if self.episodeCount == 0:
            return self.defaultValue
        return self.totalObservedReward / self.episodeCount

    @property
    def estimatedDepthRemaining(self):
        if self.episodeCount == 0:
            return -1
        return self.totalSubActionsTaken / self.episodeCount

    @property
    def mostTakenActions(self):
        """This does NOT have the same behavior as Counter.most_common(n). This will always return a list of actions
        that are most taken. so, if the counts are {a: 1, b:2, c:2} then this would return [b,c] and if the counts were
        {a:3, b:2, c:2} then it would return [a]. This way you can easily implement behavior like:
        bestAction = ss.mostTakenActions[0]
        or
        bestAction = ss.mostTakenActions[rng.choice(len(ss.mostTakenActions))]

        :return:
        """
        if self._mostTakenActions is not None:
            return self._mostTakenActions

        mostTakenActions = []
        bestValue = None
        for action in self.actionCounts:
            value = self.actionCounts[action]
            if value == bestValue:
                mostTakenActions.append(action)
            elif bestValue is None or value > bestValue:
                mostTakenActions = [action]
                bestValue = value
        self._mostTakenActions = mostTakenActions
        return mostTakenActions

    @property
    def bestValueActions(self):
        if self._bestActions is not None:
            return self._bestActions

        bestActions = []
        bestValue = None

        for action, value in self.actionValues.items():
            actionValue = value * self.stateId.polarity / self.actionCounts[action]

            if actionValue == bestValue:
                bestActions.append(action)
            elif bestValue is None or (actionValue is not None and actionValue > bestValue):
                bestActions = [action]
                bestValue = actionValue
        self._bestActions = bestActions
        return bestActions


class StateStatManager:
    def __init__(self, trackOnCreateDefault=False):
        self.stateStats = {}
        self.trackOnCreateDefault = trackOnCreateDefault

    def __getitem__(self, item: StateId):
        return self.getStateStats(item)

    def __delitem__(self, key: StateId):
        del self.stateStats[key]

    def __iter__(self):
        return self.iterkeys()

    def iterkeys(self):
        return self.stateStats.iterkeys()

    def __contains__(self, item: StateId):
        return item in self.stateStats

    def resetStates(self):
        self.stateStats = {}

    def getStateStats(self, stateId: StateId, **kwargs) -> StateStatistics:
        if stateId in self.stateStats:
            return self.stateStats[stateId]
        ss = self._constructStateStatistic(stateId)
        if kwargs.get('trackOnCreate', self.trackOnCreateDefault):
            self.stateStats[stateId] = ss
        return ss

    def _constructStateStatistic(self, stateId: StateId) -> StateStatistics:
        return NotImplemented

class State:
    """This maintains the changing state as an episode is evaluated. It has a simple dirty mechanism to support
    lazy loading and caching of values. actionsAvailable, currentStateId and value are all cached and only recalculated
    if the object is declared dirty. The object is automatically declared dirty when performAction is called. It can
    also be manually dirtied by calling 'dirty()'

    """

    def __init__(self, **kwargs):
        self.actionsTaken = []
        self._checkpointActionsTaken = []
        self.initialPolarity = kwargs.get('polarity', 1.0)
        self.polarity = self.initialPolarity
        self.polarized = kwargs.get('polarized', False)
        self.__currentStateIdClean = False
        self.__currentStateId = None

        self.__valueClean = False
        self.__value = None

        self.__availableActionsClean = False
        self.__availableActions = None
        self.atCheckpoint = True

    def _dirty(self):
        self.__currentStateIdClean = False
        self.__valueClean = False
        self.__availableActionsClean = False
        self.atCheckpoint = False

    def _performAction(self, action):
        """Guaranteed to be dirty here. The action has been appended to the performed actions.

        :return:
        """
        pass

    def performAction(self, action):
        """Simply performs the action given. When implementing this it is not necessary for the next state to be
        deterministic. So, being in the same state and performing the same action may transition you to new states.
        Polarization will switch (if polarized) and the action will be appeneded to 'actionsTaken' AFTER
        _performAction is called.

        :param action:
        """
        self._dirty()
        self._performAction(action)
        self.actionsTaken.append(action)
        if self.polarized:
            self.polarity = self.polarity * -1
        return self

    def _reset(self, oldActionsTaken):
        """
        Use this to implement custom reset logic. At this point the base class is completely reset.
        :return:
        """
        pass

    def _checkpoint(self, newState):
        return newState

    def checkpoint(self, alwaysClone=True):
        """ Creates a new state that is 'checkpointed' to the current state.

        :return:
        """
        if self.atCheckpoint and not alwaysClone:
            return self


        newState = copy.copy(self)
        newState._checkpointActionsTaken = self.actionsTaken[:]
        newState.actionsTaken = self.actionsTaken[:]
        newState.initialPolarity = self.polarity
        newState.atCheckpoint = True
        return self._checkpoint(newState)

    def reset(self):
        """ Reset to the the checkpoint for this state
        :return:
        """
        if self.atCheckpoint:
            return self
        self._dirty()
        oldActionsTaken = self.actionsTaken
        self.polarity = self.initialPolarity
        self.actionsTaken = self._checkpointActionsTaken[:]
        self.atCheckpoint=True
        self._reset(oldActionsTaken)
        return self

    def _currentValue(self):
        """ Override this method to get value. Will only be called if the object has been dirtied so goahead and make
        this an expensive operation, its value will be cached. The un-polarized value should be sent back since all
        state statistics will automatically have the correct polarization applied.

        :return:
        """
        return NotImplemented

    @property
    def currentValue(self):
        """This must return a value if there are no available actions.

        :param stateId:
        :return:
        """
        if not self.__valueClean:
            self.__value = self._currentValue()
            self.__valueClean = True
        return self.__value

    def _availableActions(self):
        return ()

    @property
    def availableActions(self):
        """

        :param stateId:
        :return: actions or empty tuple if this is a terminal state.
        """
        if not self.__availableActionsClean:
            self.__availableActions = self._availableActions()
            self.__availableActionsClean = True
        return self.__availableActions

    def _currentStateId(self):
        """This state Id work if all transitions are deterministic. If they are not then this should be swapped with
        something else.

        :return:
        """
        return StateId(tuple(action for action in self.actionsTaken), polarity=self.polarity)

    @property
    def currentStateId(self):
        if not self.__currentStateIdClean:
            self.__currentStateId = self._currentStateId()
            self.__currentStateIdClean = True
        return self.__currentStateId

    @property
    def isTerminalState(self):
        return len(self.availableActions) == 0


class Policy:
    def __init__(self, **kwargs):
        self.name = kwargs.get('name', 'Policy')

    def __str__(self):
        return self.name

    def nextAction(self, stateStatsManager: StateStatManager, state: State, **kwargs):
        """

        :param stateStats:
        :return: the next action or None if there are no more actions to take and the state should be evaluated
        """
        return NotImplemented
class RandomPolicy(Policy):
    def __init__(self, **kwargs):
        kwargs['name'] = kwargs.get('name', 'RP()')
        super().__init__(**kwargs)
        self.rng = kwargs.get('rng', np.random)

    def nextAction(self, stateStatManager, state, **kwargs):
        return choice(state.availableActions, self.rng)


class Metric:
    """Metrics return values between 0 and 1 inclusive. They cache their value in the state stats

    """
    def __init__(self, **kwargs):
        self.name = kwargs.get('name', "Metric")

    def peek(self, stateStats: StateStatistics, **kwargs):
        return stateStats.heuristicValues.get(self.name, None)

    def _evaluate(self, state: State, stateStatManager: StateStatManager, **kwargs):
        return NotImplemented

    def evaluate(self, state: State, stateStatManager: StateStatManager, **kwargs):
        value = self.peek(stateStatManager[state.currentStateId], **kwargs)
        if value is not None:
            return value
        value = self._evaluate(state, stateStatManager, **kwargs)
        stateStatManager[state.currentStateId].heuristicValues[self.name] = value
        return value

    def __str__(self):
        return self.name


class Heuristic(Metric):
    def __init__(self, **kwargs):
        kwargs['name'] = kwargs.get('name', Heuristic)
        super().__init__(**kwargs)

    def _evaluate(self, state: State, StateStatManager, **kwargs):
        """

        :param state:
        :param StateStatManager:
        :param kwargs:
        :return: value, minDepthRemaining
        """
        return 0, 0
