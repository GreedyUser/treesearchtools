from treesearchtools.baseclasses import Heuristic, State, StateStatManager, RandomPolicy, Metric


__author__ = 'GreedyUser'


class PolicyHeuristic(Heuristic):
    def __init__(self, **kwargs):
        self.policy = kwargs.get('policy', RandomPolicy())
        self.truncationDepth = kwargs.get('truncationDepth', None)
        self.truncationHeuristic = kwargs.get('truncationHeuristic', None)
        self.defaultRollouts = kwargs.get('defaultRollouts', 1)
        name = "PH({},{})".format(self.policy, self.defaultRollouts)
        if 'truncationDepth' in kwargs:
            name = "{},{},{})".format(name[:-1], self.truncationDepth, self.truncationHeuristic)
        kwargs['name'] = kwargs.get('name', name)
        super().__init__(**kwargs)

    def _evaluate(self, state: State, stateStatManager: StateStatManager, **kwargs):
        rollouts = kwargs.get('rollouts', self.defaultRollouts)
        total = 0.0
        state = state.checkpoint(alwaysClone=False)
        totalDepth = 0
        for i in range(rollouts):
            depth = 0
            while not state.isTerminalState and (self.truncationDepth is None or depth <= self.truncationDepth):
                action = self.policy.nextAction(stateStatManager, state)
                state.performAction(action)
                depth += 1
            if state.isTerminalState:
                total += state.currentValue
            else:
                val, hDepth = self.truncationHeuristic.evaluate(state, stateStatManager, **kwargs)
                total += val
                totalDepth += hDepth
            totalDepth += depth
            state.reset()
        return total / rollouts, totalDepth / rollouts


class TruncatedPolicyHeuristic(PolicyHeuristic):
    def __init__(self, heuristic: Heuristic, truncationDepth, **kwargs):
        kwargs['name'] = kwargs.get('name', "TPH({},{})".format(heuristic, truncationDepth))
        super().__init__(**kwargs)
        self.heuristic = heuristic
        self.truncationDepth = truncationDepth

    def _evaluate(self, state: State, stateStatManager: StateStatManager, **kwargs):
        rollouts = kwargs.get('rollouts', self.defaultRollouts)
        total = 0.0
        state = state.checkpoint()
        depth = 0
        for i in range(rollouts):
            depth = 0
            while not state.isTerminalState and depth < self.truncationDepth:
                action = self.policy.nextAction(stateStatManager, state)
                state.performAction(action)
                depth += 1
            if state.isTerminalState:
                total += state.currentValue
            else:
                val, simDepth = self.heuristic.evaluate(state, stateStatManager, **kwargs)
                total += val
                depth += simDepth
            state.reset()
        return total / rollouts, depth / rollouts

class FixedHeuristic(Heuristic):
    def __init__(self, **kwargs):
        self.value = kwargs.get('value', 1.0)
        if 'value' in kwargs:
            name = "FH({})".format(self.value)
        else:
            name = "FH()"

        kwargs['name'] = kwargs.get('name', name)
        super().__init__(**kwargs)

    def _evaluate(self, state: State, StateStatManager, **kwargs):
        return self.value, 0

class MixedHeuristic(Heuristic):
    def __init__(self, heuristics, weights, **kwargs):
        name = "MH("
        for heuristic, weight in zip(heuristics, weights):
            name = "{}{}*{},".format(name, weight, heuristic)
        name = "{})".format(name)

        kwargs['name'] = kwargs.get('name', name)
        super().__init__(**kwargs)
        self.heuristics = heuristics
        self.weights = weights

    def _evaluate(self, state: State, stateStatManager: StateStatManager, **kwargs):
        total = 0.0
        state = state.checkpoint()
        depth = 0
        for heuristic, weight in zip(self.heuristics, self.weights):
            val, simDepth = heuristic.evaluate(state, stateStatManager, **kwargs)
            total += val * weight
            depth += simDepth
            state.reset()
        return total, depth / len(self.heuristics)

class InvertedMetric(Metric):
    def __init__(self, metric, **kwargs):
        name = "IM({})".format(metric)
        kwargs['name'] = kwargs.get('name', name)
        super().__init__(**kwargs)
        self.metric = metric

    def _evaluate(self, state: State, stateStatManager: StateStatManager, **kwargs):
        return 1.0 - self.metric.evaluate(state, stateStatManager, **kwargs)

class FixedMetric(Metric):
    def __init__(self, **kwargs):
        self.value = kwargs.get('value', 1.0)
        if 'value' in kwargs:
            name = "FM({})".format(self.value)
        else:
            name = "FM()"

        kwargs['name'] = kwargs.get('name', name)
        super().__init__(**kwargs)

    def _evaluate(self, state: State, StateStatManager, **kwargs):
        return self.value
