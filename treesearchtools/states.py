from treesearchtools.baseclasses import StateStatManager, StateStatistics, State

__author__ = 'GreedyUser'



class SimpleStateStatManager(StateStatManager):
    def _constructStateStatistic(self, stateId) -> StateStatistics:
        return StateStatistics(stateId)




class FunctionState(State):
    """This takes a function's parameters and turns them into tree searchable options.

    """

    def __init__(self, evalFunction, **kwargs):
        super().__init__(**kwargs)
        self.argsList = kwargs.get('argsList', ())
        self.kwArgsList = kwargs.get('kwArgsList', ())
        self.currentParameter = 0
        self.evalFunction = evalFunction

    def _availableActions(self):
        actionsTakenCount = len(self.actionsTaken)
        if actionsTakenCount < len(self.argsList):
            return self.argsList[actionsTakenCount]
        elif actionsTakenCount < len(self.argsList) + len(self.kwArgsList):
            return self.kwArgsList[actionsTakenCount - len(self.argsList)][1]
        return super()._availableActions()

    def getArgs(self):
        kwargs = {}
        args = self.actionsTaken[0:len(self.argsList)]
        for idx, val in enumerate(self.actionsTaken[len(self.argsList):]):
            kwargs[self.kwArgsList[idx][0]] = val
        return args, kwargs

    def _currentValue(self):
        args, kwargs = self.getArgs()
        return self.evalFunction(*args, **kwargs)


