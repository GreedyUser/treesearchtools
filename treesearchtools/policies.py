import math

import numpy as np

from treesearchtools.baseclasses import StateStatManager, State, Policy, RandomPolicy, Heuristic
from treesearchtools.utils import choice, softmax
from treesearchtools.heuristics import FixedMetric

__author__ = 'GreedyUser'


class GreedyPolicy(Policy):
    def __init__(self, **kwargs):
        self.defaultGreedType = kwargs.get('defaultGreedType', 'value')
        self.rnd = kwargs.get('rnd', np.random)
        kwargs['name'] = kwargs.get('name', 'GP({})'.format(self.defaultGreedType))
        super().__init__(**kwargs)

    def nextAction(self, stateStatsManager: StateStatManager, state: State, **kwargs):
        ss = stateStatsManager[state.currentStateId]
        if kwargs.get('greedType', self.defaultGreedType) == 'most':
            actions = ss.mostTakenActions
        else:
            actions = ss.bestValueActions
        if len(actions) > 0:
            return choice(actions, self.rnd)
        return choice(state.availableActions, self.rnd)


class EGreedyPolicy(RandomPolicy):
    def __init__(self, e, **kwargs):
        kwargs['name'] = kwargs.get('name', "eGP({:0.3f})".format(e))
        super().__init__(**kwargs)
        self.e = e

    def nextAction(self, stateStatsManager, state, **kwargs):
        if self.rng.random_sample() <= self.e:
            return choice(state.availableActions, self.rng)
        actions = stateStatsManager.getStateStats(state.currentStateId).bestValueActions
        if len(actions) > 0:
            return choice(actions, self.rng)
        return choice(state.availableActions, self.rng)


class StochasticHeuristicPolicy(RandomPolicy):
    def __init__(self, heuristic: Heuristic, **kwargs):
        self.heuristic = heuristic
        self.usePolarity = kwargs.get('usePolarity', False)
        if 'usePolarity' in kwargs:
            name = "sHP({}, {})".format(self.heuristic, self.usePolarity)
        else:
            name = "sHP({})".format(self.heuristic)

        self.beta = kwargs.get('beta', 1.0)
        if 'beta' in kwargs:
            name = "{}, {:0.3f})".format(name[:-1], self.beta)

        kwargs['name'] = kwargs.get('name', name)
        super().__init__(**kwargs)


    def nextAction(self, stateStatsManager, state, **kwargs):
        cleanState = state.checkpoint()
        actions = []
        vals = []
        for action in state.availableActions:
            actions.append(action)

            val, _ = self.heuristic.evaluate(cleanState.reset().performAction(action), stateStatsManager, **kwargs)
            if self.usePolarity:
                val *= state.polarity
            vals.append(val)

        return choice(actions, self.rng, softmax(vals, self.beta))



class GPUCTPolicy(RandomPolicy):
    """This policy implements the variation of the PUCT algoritth described int he AlphaGo paper.

     Any deviations from the above reference or errors are completely my own.
    """

    def __init__(self, **kwargs):
        """

        :param priorHeuristic: Heuristic used to determine the prior distribution.
        :param rng: send in an instance of numpy.RandomState to allow repeatable behavior.
        :param c: controls exploration. Higher values encourage more exploration
        :param beta: temperature. Higher values minimize the impact of the prior.
        :param kwargs:
        """

        name = 'GPUCTP( '

        self.priorMetric = kwargs.get('priorMetric', FixedMetric())
        if 'priorMetric' in kwargs:
            name = "{}{},".format(name, self.priorMetric)

        self.c = kwargs.get('c', 1.0 / (2.0 ** .5))
        if 'c' in kwargs:
            name = "{}{:0.3f},".format(name, self.c)

        self.beta = kwargs.get('beta', 1.0)
        if 'beta' in kwargs:
            name = "{}{:0.3f},".format(name, self.beta)



        name = "{})".format(name[:-1])
        kwargs['name'] = kwargs.get('name', name)
        super().__init__(**kwargs)

    def prior(self, stateStats, stateStatsManager: StateStatManager, cleanState: State):
        vals = []
        actions = []
        for action in cleanState.availableActions:
            actions.append(action)
            cleanState.reset().performAction(action)
            vals.append(self.priorMetric.evaluate(cleanState.checkpoint(), stateStatsManager))

        p = {action: value for action, value in zip(actions, softmax(vals, self.beta))}
        return p

    def nextAction(self, stateStatsManager: StateStatManager, state: State, **kwargs):
        if len(state.availableActions) == 1:
            return state.availableActions[0]

        cleanState = state.checkpoint()
        stateStats = stateStatsManager[state.currentStateId]
        unvisitedActions = stateStats.unvisitedActions(state.availableActions)
        if len(unvisitedActions) > 0:
            return choice(unvisitedActions, self.rng)

        p = self.prior(stateStats, stateStatsManager, cleanState)

        # GPUCT choice for S = argmax(A){ Q(S,A) + u(S,A)}
        # u(S,A) = c*P(s,a)*(T(S)**.5/(1 + N(S,A))
        # Where:
        # S=State
        # A=Action
        # Q(S,A) = the current estimated value of the state action pair
        # N(S,A) = the number of episodes that have been evaluated for the given state action
        # T(S) = the total number of actions that have been evaluated for this state
        # c = the exploration constant

        # There are two major differences between GPUCT and UCB1. First, the boost grows based on the square root
        # of the total simulations instead of the square root of the log. This should encourage widers searches
        # since the growth of the boost for lower confidence areas will be faster. The second major change is the
        # addition of a prior distribution to the boost. The prior distribution will initially encourage exploration
        # based on the supplied distribution but have less effect as more examples are seen.


        t = stateStats.episodeCount
        bestAction = None
        bestValue = None

        for action in state.availableActions:

            n = stateStats.actionCounts[action]
            q = stateStats.estimatedQ(action, valueDefault=0)
            ucvBoost = p[action] * math.sqrt(t) * self.c / n
            actionValue = q + ucvBoost * stateStats.stateId.polarity

            if stateStats.stateId.polarity > 0:
                if bestAction is None or actionValue > bestValue:
                    bestValue = actionValue
                    bestAction = [action]
                elif actionValue == bestValue:
                    bestAction.append(action)
            else:
                if bestAction is None or actionValue < bestValue:
                    bestValue = actionValue
                    bestAction = [action]
                elif actionValue == bestValue:
                    bestAction.append(action)

        return choice(bestAction, self.rng)


class UCB1Policy(RandomPolicy):
    """
    Much of the below code is based on the following paper:
     Browne, C. B., Powley, E., Whitehouse, D., Lucas, S. M., Cowling, P. I., Rohlfshagen, P., … Colton, S. (2012).
     A Survey of Monte Carlo Tree Search Methods. Computational Intelligence and AI in Games, IEEE Transactions on, 4(1), 1–43.
     https://doi.org/10.1109/TCIAIG.2012.2186810

     Any deviations from the above reference or errors are completely my own.
    """

    def __init__(self, **kwargs):
        """
        :param rng: send in an instance of numpy.RandomState to allow repeatable behavior.
        :param c: controls exploration. Higher values encourage more exploration
        :param wideSearch: if true (default) all available actions are tried at least once before UCB kicks in. if false then unvisited actions are assumed to have an average value until tried once.
        :param kwargs:
        """
        name = 'UCB1P('
        self.c = kwargs.get('c', 1.0 / (2.0 ** .5))
        if 'c' in kwargs:
            name = "{}{:0.3f}".format(name, self.c)

        self.cFunc = kwargs.get('cFunc', lambda c, d: c)
        if 'cFunc' in kwargs:
            name = "{},cFunc".format(name)

        self.wideSearch = kwargs.get('wideSearch', True)
        if 'wideSearch' in kwargs:
            name = "{},{}".format(name, self.wideSearch)

        name = "{})".format(name)

        kwargs['name'] = kwargs.get('name', name)
        super().__init__(**kwargs)

    def nextAction(self, stateStatsManager, state: State, **kwargs):
        if len(state.availableActions) == 1:
            return state.availableActions[0]

        stateStats = stateStatsManager.getStateStats(state.currentStateId)
        if stateStats.episodeCount <= 1:
            return choice(state.availableActions, self.rng)

        # If wide search is on then randomly explore unvisited states until all states have been visited once.
        unvisitedActions = stateStats.unvisitedActions(state.availableActions)
        if self.wideSearch and len(unvisitedActions) > 0:
            return choice(unvisitedActions, self.rng)

        # UCB1 choice for S = argmax(A){ Q(S,A) + (2*ln(T(S))/N(S,A))^-2 }
        # Where:
        # S=State
        # A=Action
        # Q(S,A) = the current estimated value of the state action pair
        # N(S,A) = the number of episodes that have been evaluated for the given state action
        # T(S) = the total number of actions that have been evaluated for this state

        # UCB1 does a great job of exploration vs exploitation, but the default initial wide search policy could have
        # an alternate implementation. Instead of trying all possible actions once I assume that values not yet
        # uncovered have the current average value until they are explored once. This means that after the first
        # evaluation all values look the same but after two things will start to differentiate. I believe this approach
        # does a good job of maintaining explore/exploit in low episode branches. Some preliminary testing appears
        # to back this up. Since this is not a well evaluated modification to the algorithm this behavior is off by
        # default and this logic only kicks in if wideSearch = False


        # By adding the unvisited count to t we get rid of log(0). It also somewhat makes sense since we are faking values
        # for the unvisited states
        t = stateStats.episodeCount + len(unvisitedActions)
        numerator = 2.0 * math.log(t)
        bestAction = None
        bestValue = None

        for action in state.availableActions:

            # Since we are simulating that every action has a value, n must be >= 0. This also gets rid of
            # div by zero errors.
            n = stateStats.actionCounts[action]
            if n == 0:
                n = 1
            q = stateStats.estimatedQ(action, valueDefault=0)
            c = self.cFunc(self.c, len(state.actionsTaken))
            ucvBoost = math.sqrt(numerator / n) * c
            actionValue = q + ucvBoost * stateStats.stateId.polarity
            if stateStats.stateId.polarity > 0:
                if bestAction is None or actionValue > bestValue:
                    bestValue = actionValue
                    bestAction = [action]
                elif actionValue == bestValue:
                    bestAction.append(action)
            else:
                if bestAction is None or actionValue < bestValue:
                    bestValue = actionValue
                    bestAction = [action]
                elif actionValue == bestValue:
                    bestAction.append(action)

        return choice(bestAction, self.rng)
