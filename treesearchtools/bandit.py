import time

from treesearchtools.baseclasses import State
from treesearchtools.heuristics import PolicyHeuristic
from treesearchtools.policies import GreedyPolicy, UCB1Policy
from treesearchtools.states import SimpleStateStatManager


class MonteCarloManager(SimpleStateStatManager):
    """
    Much of the below code is based on the following paper:
     Browne, C. B., Powley, E., Whitehouse, D., Lucas, S. M., Cowling, P. I., Rohlfshagen, P., … Colton, S. (2012).
     A Survey of Monte Carlo Tree Search Methods. Computational Intelligence and AI in Games, IEEE Transactions on, 4(1), 1–43.
     https://doi.org/10.1109/TCIAIG.2012.2186810

     Any deviations from the above reference or errors are completely my own.
    """

    def __init__(self, **kwargs):
        super().__init__()

        name = "MCM("
        self.defaultSelectionPolicy = kwargs.get('defaultSelectionPolicy', UCB1Policy())
        if 'defaultSelectionPolicy' in kwargs:
            name = "{},dsp:{}".format(name, self.defaultSelectionPolicy)

        self.defaultMinEpisodes = kwargs.get('defaultMinEpisodes', 1)
        if 'defaultMinEpisodes' in kwargs:
            name = "{},dme:{}".format(name, self.defaultMinEpisodes)

        self.defaultMaxEpisodes = kwargs.get('defaultMaxEpisodes', None)
        if 'defaultMaxEpisodes' in kwargs:
            name = "{},dme:{}".format(name, self.defaultMaxEpisodes)

        self.defaultMaxTime = kwargs.get('defaultMaxTime', None)
        if 'defaultMaxTime' in kwargs:
            name = "{},dmt:{}".format(name, self.defaultMaxTime)

        self.defaultHeuristic = kwargs.get('defaultHeuristic', PolicyHeuristic())
        if 'defaultHeuristic' in kwargs:
            name = "{},dh:{}".format(name, self.defaultHeuristic)

        self.defaultEpisodeSkew = kwargs.get('defaultEpisodeSkew', 1.0)
        if 'defaultEpisodeSkew' in kwargs:
            name = "{},des:{}".format(name, self.defaultEpisodeSkew)

        self.defaultMaximumExpansionDepth = kwargs.get('defaultMaximumExpansionDepth', None)
        if 'defaultMaximumExpansionDepth' in kwargs:
            name = "{},dmed:{}".format(name, self.defaultMaximumExpansionDepth)

        self.name = "{})".format(name)
        self.mostPolicy = GreedyPolicy(defaultGreedType='most')
        self.bestPolicy = GreedyPolicy(defaultGreedType='value')

    def __str__(self):
        return self.name

    def generateEpisodes(self, state: State, **kwargs):
        cleanState = state.checkpoint()

        minEpisodes = kwargs.get('minEpisodes', self.defaultMinEpisodes)
        maxEpisodes = kwargs.get('maxEpisodes', self.defaultMaxEpisodes)
        maxTime = kwargs.get('maxTime', self.defaultMaxTime)

        totalEpisodes = 0

        startTime = time.time()

        while totalEpisodes < minEpisodes:
            self.generateEpisode(cleanState.reset(), **kwargs)
            totalEpisodes += 1

        while (maxEpisodes is None or totalEpisodes < maxEpisodes) and (
                        maxTime is None or time.time() < startTime + maxTime):
            self.generateEpisode(cleanState.reset(), **kwargs)
            totalEpisodes += 1

        return totalEpisodes

    def fixedGenerateEpisodes(self, state: State, **kwargs):
        minEpisodes = kwargs.get('minEpisodes', self.defaultMinEpisodes)
        maxEpisodes = kwargs.get('maxEpisodes', self.defaultMaxEpisodes)
        maxTime = kwargs.get('maxTime', self.defaultMaxTime)
        episodeSkew = kwargs.get('episodeSkew', self.defaultEpisodeSkew)

        cleanState = state.checkpoint()
        totalEpisodes = 0
        startTime = time.time()
        while totalEpisodes < minEpisodes:
            self.generateEpisode(cleanState.reset(), **kwargs)
            totalEpisodes += 1

        cleanState.reset()
        previousTransitions = []
        while not cleanState.isTerminalState and \
                ((maxEpisodes is None or totalEpisodes < maxEpisodes) and (
                                maxTime is None or time.time() < startTime + maxTime)):
            stateStat = self.getStateStats(cleanState.currentStateId, trackOnCreate=True)
            if maxEpisodes is None:
                argMaxEpisodes = None
            else:
                argMaxEpisodes = int((maxEpisodes - totalEpisodes) / (stateStat.estimatedDepthRemaining * episodeSkew))
            if maxTime is None:
                argMaxTime = None
            else:
                argMaxTime = (startTime + maxTime - time.time()) / (stateStat.estimatedDepthRemaining * episodeSkew)
                if argMaxTime < 0:
                    argMaxTime = 0

            kwargs['maxTime'] = argMaxTime
            kwargs['maxEpisodes'] = argMaxEpisodes
            kwargs['minEpisodes'] = 1
            kwargs['previousTransitions'] = previousTransitions
            totalEpisodes += self.generateEpisodes(cleanState, **kwargs)
            action = stateStat.bestValueActions[0]
            oldStateId = cleanState.currentStateId
            cleanState = cleanState.performAction(action).checkpoint()
            previousTransitions.append((oldStateId, action, cleanState.currentStateId))
        return totalEpisodes

    def generateEpisode(self, state: State, **kwargs):

        selectionPolicy = kwargs.get('selectionPolicy', self.defaultSelectionPolicy)
        maximumExpansionDepth = kwargs.get('maximumExpansionDepth', self.defaultMaximumExpansionDepth)
        heuristic = kwargs.get('heuristic', self.defaultHeuristic)
        previousTransitions = kwargs.get('previousTransitions', [])

        currentDepth = 0
        transitions = []
        currentStateStats = self.getStateStats(state.currentStateId, trackOnCreate=True)
        expanded = currentStateStats.expanded
        kwargs['transitions'] = transitions
        kwargs['initialState'] = state.checkpoint()

        # Selection/Tree and expansion
        while not state.isTerminalState and expanded and \
                (maximumExpansionDepth is None or currentDepth <= maximumExpansionDepth):
            currentDepth += 1
            startStateStats = currentStateStats
            currentAction = selectionPolicy.nextAction(self, state, **kwargs)
            state.performAction(currentAction)
            currentStateStats = self.getStateStats(state.currentStateId, trackOnCreate=True)
            transitions.append((startStateStats.stateId, currentAction, currentStateStats.stateId))
            # Expansion
            if not currentStateStats.expanded:
                currentStateStats.expanded = True
                expanded = False

        # Simulation/rollout
        if state.isTerminalState:
            observedReward = state.currentValue
            heuristicDepth = 0
        else:
            # the default heuristic estimates with a random rollout.
            observedReward, heuristicDepth = heuristic.evaluate(state, self, **kwargs)

        # backprop
        for depth, (stateId, action, resultingStateId) in enumerate(transitions):
            self[stateId].update(observedReward, currentDepth - depth + heuristicDepth, actionTaken = action)

        for depth, (stateId, action, resultingStateId) in enumerate(previousTransitions):
            self[stateId].update(observedReward,
                                 currentDepth + len(previousTransitions) - depth + heuristicDepth, actionTaken = action)
        #The newly expanded state gets an update but since we don't track rollout actions/transitions just update the
        #estimated value
        currentStateStats.update(observedReward, heuristicDepth)
        currentStateStats.expanded = True
        return transitions

    def getPath(self, state, policy):
        actions = []
        while not state.isTerminalState:
            action = policy.nextAction(self, state)
            state.performAction(action)
            actions.append(action)
        return actions

    def mostTakenPath(self, state):
        return self.getPath(state, self.mostPolicy)

    def bestPath(self, state):
        return self.getPath(state, self.bestPolicy)


class MultiArmedBandit(MonteCarloManager):
    """A multi-armed bandit is a slot machine. The MAB problem considers a row of bandits each with an unknown
    distribution. The goal is to get the most money out of the machines after n pulls. This class allows exploring
    that problem. A MAB is a special case of MCTS where no expansion is allowed. This means the selection policy
    will only be used for the first choice and the value from there will be estimated by rollout and/or truncation

    """

    def __init__(self, **kwargs):
        kwargs['defaultMaximumExpansionDepth'] = 0
        super().__init__(**kwargs)
