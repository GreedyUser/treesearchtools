import random
import numpy as np

import treesearchtools as tst


def funcGenerator(argsList, kwArgsList):
    maxArgValues = [np.max(a) for a in argsList]
    maxkwArgsValues = [np.max(a[1]) for a in kwArgsList]
    max = 1.0 * np.prod(maxArgValues) * np.prod(maxkwArgsValues)
    return lambda *args, **kwargs: (np.prod(args) * np.prod(list(kwargs.values()))) / max


class SimpleMABState(tst.State):
    def __init__(self, actions, rnd=np.random):
        super().__init__()
        self.__actions = actions
        self.rnd = rnd

    def _currentValue(self):
        if len(self.actionsTaken) > 0:
            return self.rnd.rand() * self.actionsTaken[0] / np.max(self.__actions)
        return 0

    def _availableActions(self):
        if len(self.actionsTaken) == 0:
            return self.__actions
        return ()


def mabExample():
    print("MAB")
    mab = tst.MultiArmedBandit()
    state = SimpleMABState(tuple(range(1, 10)))
    stateId = state.currentStateId
    for x in range(50):
        mab.generateEpisode(state.reset())
        stateStats = mab[stateId]
        if len(state.actionsTaken) > 0:
            print("Trial({}) Action: {}, estimatedValue: {}, bestValueActions: {}".format(x, state.actionsTaken[0],
                                                                                          stateStats.estimatedValue,
                                                                                          stateStats.bestValueActions))


def shuffle(x):
    random.shuffle(x)
    return x


def mctsExample():
    print("MCTS")
    MAX = 10.0
    NUM_ARGS = 10

    # Randomize the paramters to remove the posibility of detecting order.
    argsList = tuple(shuffle(list(np.arange(1.0, MAX, 1.0))) for i in range(NUM_ARGS))
    kwArgsList = tuple((argName, shuffle(list(np.arange(1.0, MAX, 1.0)))) for argName in ('y', 'z'))
    evalFunc = funcGenerator(argsList, kwArgsList)
    state = tst.FunctionState(evalFunc, argsList=argsList, kwArgsList=kwArgsList)
    mcm = tst.MonteCarloManager()

    mcm.generateEpisodes(state, maxEpisodes=100)
    # for episode in range(100):
    #    mcm.generateEpisode(state.reset())
    #    print("Args: {}".format(state.currentStateId))

    print("Best args: {}".format(mcm.bestPath(state.reset())))

    print("Most tatken args: {}".format(mcm.mostTakenPath(state.reset())))

    print("Estimated depth: {}".format(mcm[state.reset().currentStateId].estimatedDepthRemaining))


def twoPlayerMCTSExample(fixed=False):
    if fixed:
        print("Two player Fixed MCTS")
    else:
        print("Two player MCTS")
    MAX = 10.0
    NUM_ARGS = 10

    # Randomize the paramters to remove the posibility of detecting order.
    argsList = tuple(shuffle(list(np.arange(1.0, MAX, 1.0))) for i in range(NUM_ARGS))
    kwArgsList = tuple((argName, shuffle(list(np.arange(1.0, MAX, 1.0)))) for argName in ('y', 'z'))
    evalFunc = funcGenerator(argsList, kwArgsList)

    # The only difference is that the state is polarized.
    state = tst.FunctionState(evalFunc, argsList=argsList, kwArgsList=kwArgsList, polarized=True)
    mcm = tst.MonteCarloManager()
    if fixed:
        mcm.fixedGenerateEpisodes(state, maxEpisodes=1000)
    else:
        mcm.generateEpisodes(state, maxEpisodes=1000)

        # for episode in range(5000):
        #    mcm.generateEpisode(state.reset())
        # print("Args: {}".format(state.currentStateId))

    # Notice how the beginning args are generally ok, but quickly they become basically random. It just can't dig deep.
    # Bumping up the episode count barely improves this.
    print("Best args: {}".format(mcm.bestPath(state.reset())))

    print("Most tatken args: {}".format(mcm.mostTakenPath(state.reset())))

    print("Estimated depth: {}".format(mcm[state.reset().currentStateId].estimatedDepthRemaining))


def main():
    mabExample()
    mctsExample()
    twoPlayerMCTSExample(fixed=False)
    twoPlayerMCTSExample(fixed=True)


if __name__ == "__main__":
    main()
