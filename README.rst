Copyright 2017 Jeff Ward

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
either express or implied. See the License for the specific
language governing permissions and limitations under the License.

These tools were created to help me explore many ideas. They have *experimental* ideas in them and are not well aligned
with any particular paper or peer reviewed implementation. In other words, *use at your own risk*!

This tool set implements simple bandit algorithms, a plugable MCTS implementation and some custom concepts I have
coded. So far this project has been used to test ideas for tuning hyper parameters, playing
simple two player games and playing with ideas around truncation (value functions) and roll-out policies. It is in flux
as I play with more ideas. I am just now starting to focus on code quality so I expect it to become more generic and
readable shortly.

Some of the things contained in this code are:

- *Multi armed bandit:*
  This is the starting point that most math begins with. It is really useful to play with this simple version first to explore different selection policies before moving to more complex situations.

- *Monte Carlo Tree Search:*
  This is one of the most successful recent algorithms around. A variation of it is used in AlphaGo.

- *Fixed Monte Carlo Tree Search:*
  This tests a concept I have for better state evalation with MCTS. It is NOT based on a published idea and should not be used without a deep understanding of the problem it is trying to solve and how it attacks it.

- *Random Policy:*
  A simple implementation of a random policy. Name says it all.

- *Greedy Policy:*
  A simple implementation that always takes the best available or most taken move.

- *e-Greedy Policy:*
  e-Greedy takes the best action (1.0 - e) percent of the time and the remainder of the time randomly selects the move. This is the most basic algorithm that attempts to balance exploration and exploitation.

- *UCB1 Policy:*
  Upper Confidence Bound attempts to minimize *regret*. You can think of regret as the value you missed out on by pulling one arm over the best arm. UCB1 is incredibl simple, but can have very unexpected results depending on the situation.

- *Truncated Heuristic:*
  Implements a way of *truncating* the simulation and using an estimated value instead of completing the simulation, potentially increasing the speed of simulations and potentially better describing the value of a state.
  
- *Mixed Heuristic:*
  Implements a way of mixing the values of multiple heuristics to potentially improve the simulation estimation. Combining rollout with a value based heuristic is one of the key components in AlphaGo. This allows for testing those concepts.

- *Function State:*
  Wraps a function to allow tree search to apply to its arguments. This allows for optimizing simple function parameters using MCTS.

**Acknowledgements**

Much of the code is based on the following paper:
Browne, C. B., Powley, E., Whitehouse, D., Lucas, S. M., Cowling, P. I., Rohlfshagen, P., � Colton, S. (2012). A Survey of Monte Carlo Tree Search Methods. Computational Intelligence and AI in Games, IEEE Transactions on, 4(1), 1-43. https://doi.org/10.1109/TCIAIG.2012.2186810

Additionally, the explanations provided by Jeff Bradberry with his excellent article 'Introduction to Monte Carlo Tree Search' (https://jeffbradberry.com/posts/2015/09/intro-to-monte-carlo-tree-search/) were also helpful.

**Any deviations from the above references or errors are completely my own.**


If you use this code or the ideas in it please let me know: wallyduck@yahoo.com