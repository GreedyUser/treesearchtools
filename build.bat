echo off
set projectName=treesearchtools
rmdir /s /q dist
rmdir /s /q %projectName%.egg-info
rmdir /s /q build
python setup.py clean

IF "%1"=="clean" (
echo project clean
exit /b
)
echo building
python setup.py sdist

if "%1"=="dist" (
exit /b
)

IF EXIST dist\%projectName%-%1.zip (
set regFile=dist\%projectName%-%1.zip
) ELSE (
set regFile=dist\%projectName%-%1.tar.gz
)

IF EXIST %regFile% (
echo found %regFile%
IF "%2"=="test" (
twine register -r testpypi --config-file ../.pypirc %regFile%
twine upload -r testpypi --config-file ../.pypirc --skip-existing dist/*
echo test built
)
IF "%2" == "prod" (
twine register -r pypi --config-file ../.pypirc %regFile%
twine upload -r pypi --config-file ../.pypirc --skip-existing dist/*
echo production built
)
) ELSE (
echo couldn't find file '%regFile%'
)
