""" This file shows some experimental work with what I call 'fixed' MCTS. UCT works well and has many advantages, but
 I am not sure that it is an optimal policy. The problem comes in that UCB1 assumes that each pull of the handle is
 iid, but that is not true. Each pull of the handle makes the distribution *better*, that is the point. So if you start
 with the worst handle but pull it a few times it may end up being slightly better than another handle. This means UCB1
 will give it even more pulls which, of course, makes it look even better than the other, actually better, options.

 What does this mean? First, if you are trying to estimate the true value of a state, UCT is not your friend. While it
 will technically explore the entire tree, eventually, it is very likely find a bad optimum along the way and get stuck
 there for a very long time. For an alternative strategy look at the 'fixed' scheme I have below. Run the code and look
   at the produced graph to see how much better things can be than UCT.

"""
import time

import random
import easyplottools as ept
import numpy as np

import treesearchtools as tst


def funcGenerator(argsList, kwArgsList, polarized):
    allArgs = argsList + tuple(args for name, args in kwArgsList)
    maxArgs = [np.max(args) for args in allArgs]
    polarizedArgs = []
    for idx, args in enumerate(allArgs):
        if idx % 2 == 0:
            polarizedArgs.append(np.max(args))
        else:
            polarizedArgs.append(np.min(args))

    max = 1.0 * np.prod(maxArgs)

    def simpleFunction(*args, **kwargs):
        val = (np.prod(args) * np.prod(tuple(kwargs.values())))
        return val / max

    def polarizedFunction(*args, **kwargs):
        val = 1.0
        for idx, polarizedVal in enumerate(polarizedArgs):
            if idx < len(args):
                val *= maxArgs[idx] - abs(polarizedVal - args[idx])
            else:
                diff = abs(polarizedVal - kwargs[kwArgsList[idx - len(args)][0]])
                val *= maxArgs[idx] - diff
        return val / max

    if polarized:
        return simpleFunction, polarizedFunction
    return simpleFunction, simpleFunction


def addResults(mcManager, state, bestResults, policyIdx, episode, polarizedFunction):
    mcManager.bestPath(state)
    args, kwArgs = state.getArgs()
    result = polarizedFunction(*args, **kwArgs)
    bestResults[policyIdx, episode, 1] = result
    bestResults[policyIdx, episode, 0] = episode


def runTrial(polarizedFunction, episodes, state, testPolicies, episodeSkew=1.0, maximumExpansionDepth=3, minEpisodes=1):
    bestDataSets = np.zeros((len(testPolicies) * 2, episodes, 2))

    # currentTrial = "MCTS"
    mcManager = tst.MonteCarloManager()
    for policyIdx, policy in enumerate(testPolicies):

        mcManager.resetStates()
        for x in range(episodes):
            mcManager.generateEpisode(state.reset(), selectionPolicy=policy)
            addResults(mcManager, state.reset(), bestDataSets, policyIdx, x, polarizedFunction)

    # currentTrial = "MCTSFixed"
    mcManager.resetStates()
    state.reset()
    for policyIdx, policy in enumerate(testPolicies, start=len(testPolicies)):
        cleanState = state.checkpoint()
        argCount = len(cleanState.argsList) + len(cleanState.kwArgsList)

        episodesRemaining = episodes - argCount * minEpisodes
        mcManager.resetStates()
        x = 0
        for currentArg in range(argCount):
            if (argCount - currentArg) == 1:
                subEpisodeLength = episodesRemaining + minEpisodes
            else:
                subEpisodeLength = int(episodesRemaining / ((argCount - currentArg) * episodeSkew))
                subEpisodeLength += minEpisodes

            for _ in range(subEpisodeLength):
                mcManager.generateEpisode(cleanState.reset(), selectionPolicy=policy,
                                          maximumExpansionDepth=maximumExpansionDepth)
                addResults(mcManager, cleanState.reset(), bestDataSets, policyIdx, x, polarizedFunction)
                x += 1

            episodesRemaining -= (subEpisodeLength - minEpisodes)
            stateStats = mcManager[cleanState.reset().currentStateId]
            action = stateStats.bestValueActions[0]
            cleanState.performAction(action)
            cleanState = cleanState.checkpoint()

    return bestDataSets


def shuffle(x):
    random.shuffle(x)
    return x


def fixedTests(trials, episodes, c, episodeSkew, e, wideSearch, maximumExpansionDepth, polarized, minEpisodes):
    """ This is attempting to find the best parameters for a function using variations on bandit approaches.
    While the function is simple, just multiply values together, it has a lot of similarities with tuning hyperparameters.
    The final results show that a successful strategy is to use a variation on MCTS. This variation randomly orders the
    parameters and runs a fixed number of episodes before it assumes that the distribution for at least the first parameter
    is accurate. It then fixes that value and continues testing. This method doesn't guarantee the best result, but it
    dramatically outperforms continuously running MCTS without fixing parameters. (see the graph generated in results).

    This is a simple algorithm for hyperparameter tuning that could be a good baseline for comparing to other more sophisticated
     attacks.


    :return:
    """

    MAX = 10.0
    NUM_ARGS = 10
    # Randomize the paramters to remove the posibility of detecting order.
    argsList = tuple(shuffle(list(np.arange(1.0, MAX, 1.0))) for i in range(NUM_ARGS))
    kwArgsList = tuple((argName, shuffle(list(np.arange(1.0, MAX, 1.0)))) for argName in ('y', 'z'))

    simpleFunction, polarizedFunction = funcGenerator(argsList=argsList, kwArgsList=kwArgsList, polarized=polarized)

    functionState = tst.FunctionState(simpleFunction, argsList=argsList, kwArgsList=kwArgsList, polarized=polarized)

    rng = np.random.RandomState()
    testPolicies = (
        tst.UCB1Policy(rng=rng, c=c, wideSearch=wideSearch), tst.GPUCTPolicy(), tst.RandomPolicy(rng=rng), tst.EGreedyPolicy(e=e, rng=rng))
    # testPolicies = (
    #    tst.UCB1Policy(rng=rng, c=c, wideSearch=wideSearch),)
    # testPolicies = (tst.RandomPolicy(rng=rng),tst.EGreedyPolicy(e=e, rng=rng),)

    plotLabels = ["{}.{}".format(currentTrial, policy) for currentTrial in ('MCTS', 'MCTSFixed') for policy in
                  testPolicies]

    allBestValues = np.zeros((trials, len(testPolicies) * 2, episodes, 2))

    for i in range(trials):
        startTime = time.time()
        print("Group Trial {}".format(i))
        bestDataSets = runTrial(polarizedFunction, episodes, functionState, testPolicies,
                                episodeSkew=episodeSkew, maximumExpansionDepth=maximumExpansionDepth,
                                minEpisodes=minEpisodes)
        allBestValues[i, :] = bestDataSets
        print("Time:{}".format(time.time() - startTime))

    name = "results/best-tr{}-ep{}-c{}-es{}-e{}-ws{}-d{}-p{}-me{}".format(trials, episodes, c, episodeSkew, e,
                                                                          wideSearch,
                                                                          maximumExpansionDepth, polarized, minEpisodes)
    name = name.replace('.', '')
    plotData = allBestValues.mean(axis=0)

    #    ept.plotAndSave2dSparse(plotData, name, xLabel="episode",
    #                            yLabel="value", sortByX=False)
    ept.plotAndSave2dSparse(plotData, name, labels=plotLabels, xLabel="episode",
                            yLabel="value", sortByX=False)


def main():
    trials = 5
    episodes = 12 * 100

    # More episodes makes fixed better where traditional stalls. Uncomment below to see that fixed beats UCT even on
    # lower episode counts.
    # episodes = 4 * 100
    # episodes = 1 * 100
    # c =  1.0/(2.0**.5)
    c = .000001  # 1.0/(2.0**.5)
    episodeSkew = .8
    minEpisodes = 10
    e = .1
    wideSearch = True
    maximumExpansionDepth = 3
    polarized = True

    #for episodeSkew in (.2, .4, .6, .8, 1.0):
    #for c in (1.0/(2.0**.5), .1, .01, .001, .0001, .00001):
    fixedTests(trials, episodes, c, episodeSkew, e, wideSearch, maximumExpansionDepth, polarized, minEpisodes)


if __name__ == "__main__":
    main()
